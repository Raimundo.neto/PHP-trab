<?php
    class Usuario{
        private $nome;
        private $user;
        private $email;
        private $senha;
        private $tipo;

        public function getNome(){
            return $this->nome;
        }
        public function getUser(){
            return $this->user;
        }
        public function getEmail(){
            return $this->email;
        }
        public function getSenha(){
            return $this->senha;
        }
        public function getTipo(){
            return $this->tipo;
        }
        public function setNome($n){
            $this->nome = $n;
        }
        public function setUser($u){
            $this->user = $u;
        }
        public function setEmail($e){
            $this->email = $e;
        }
        public function setSenha($s){
            $this->senha = $s;
        }
        public function setTipo($t){
            $this->tipo = $t;
        }
        
    }
?>  
