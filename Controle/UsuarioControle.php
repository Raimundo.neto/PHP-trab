<?php
require_once("Conect.php");
require_once("Modelo/Usuario.php");
    class UsuarioControle{
        function remover($id){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("DELETE FROM usuario WHERE id=:id");
                $cmd->bindParam("id", $id);
                if($cmd->execute()){
                    return true;
                }else{
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro de PDO: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
        }
        function selecionarTodos(){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("SELECT * FROM usuario;");
                $cmd->execute();
                $resultado = $cmd->fetchAll(PDO::FETCH_CLASS, "Usuario");
                return $resultado;
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        function inserir($usuario){
            try{
                $conexao = new Conexao();
                $nome = $usuario->getNome();
                $user = $usuario->getUser();
                $mail = $usuario->getEmail();
                $type = $usuario->getTipo();
                $senha = $usuario->getSenha();
                $cmd = $conexao->getConexao()->prepare("INSERT INTO usuario(nome,user,email,tipo,senha) VALUES(:n,:u,:e,:t,:s);");
                $cmd->bindParam("n", $nome);
                $cmd->bindParam("u", $user);
                $cmd->bindParam("e", $mail);
                $cmd->bindParam("t", $type);
                $cmd->bindParam("s", $senha);
                if($cmd->execute()){
                    $conexao->fecharConexao();
                    return true;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro do banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
        }
    function selecionarTipo($usuario){
        try{
            $User = $usuario->getUser();
            $conexao = new Conexao();
            $cmd = $conexao->getConexao()->prepare("SELECT tipo FROM usuario WHERE user = :u;");
            $cmd->bindParam("u", $User);
            $cmd->execute();
                $resultado = $cmd->fetchAll(PDO::FETCH_CLASS, "Usuario");
                return $resultado;
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
    function selecionarUm($usuario){
        try{
            $senha = $usuario->getSenha();
            $User = $usuario->getUser();
            $conexao = new Conexao();
            $cmd = $conexao->getConexao()->prepare("SELECT user,senha FROM usuario WHERE senha = :s AND user = :u;");
            $cmd->bindParam("s", $senha);
            $cmd->bindParam("u", $User);
            $cmd->execute();
                $resultado = $cmd->fetchAll(PDO::FETCH_CLASS, "Usuario");
                return $resultado;
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
    }
?>





