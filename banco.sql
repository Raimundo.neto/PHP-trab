DROP SCHEMA Tphp IF EXISTS;
CREATE SCHEMA Tphp IF NOT EXISTS;
CREATE TABLE Tphp.usuarioCreate table Tphp.usuario(
	id int(3) auto_increment primary key,
	nome varchar(100) not null,
	user varchar(100) not null,
	email varchar(100) not null, 
	tipo varchar(6) not null, 
	senha varchar(6) not null
);